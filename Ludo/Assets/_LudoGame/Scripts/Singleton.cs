using UnityEngine;

/// <summary>
/// MONOBEHAVIOR PSEUDO SINGLETON ABSTRACT CLASS
/// usage	: best is to be attached to a gameobject but if not that is ok,
/// 		: this will create one on first access
/// example	: '''public sealed class MyClass : Singleton<MyClass> {'''

/// </summary>
public abstract class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{

	private static T _instance = null;

	public static bool IsAwake { get { return (_instance != null); } }

	
	/// gets the instance of this Singleton
	/// use this for all instance calls:
	/// MyClass.Instance.MyMethod();
	/// or make your public methods static
	/// and have them use Instance
	public static T Instance
	{
		get
		{
			if (_instance == null)
			{
				_instance = (T)FindObjectOfType(typeof(T));
				if (_instance == null)
				{

					string goName = typeof(T).ToString();

					GameObject go = GameObject.Find(goName);
					if (go == null)
					{
						go = new GameObject();
						go.name = goName;
					}

					_instance = go.AddComponent<T>();
				}
			}
			return _instance;
		}
	}

	// for garbage collection
	public virtual void OnApplicationQuit()
	{
		// release reference on exit
		Debug.Log("Parents function call");
		_instance = null;
	}

}