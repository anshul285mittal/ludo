using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

// this is the Main Class in the project
// GameManager manage all scene change funcnality.
public class GameManager : Singleton<GameManager>
{
    private const string GAME_PLAY_SCENE_NAME = "GamePlay";
    [SerializeField] private GameObject lodingPrompt;
    [SerializeField] private Text _loadingText;

    private bool isTokenLoaded;
    private bool isDiceLoaded;
    void Awake()
    {
        if (!IsAwake)
        {
            DontDestroyOnLoad(Instance.gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
        Events.tokenLoadCompleted += TokenLoadCompleted;
        Events.diceLoadCompleted += DiceLoadCompleted;
    }
  
    public void GamePlayLoad()
    {
        StartCoroutine(GamePlayLoadIEnumerator());
    }

    //this Function specficly for GamePlay loading.
    IEnumerator GamePlayLoadIEnumerator()
    {
        
        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(GAME_PLAY_SCENE_NAME);
        lodingPrompt.SetActive(true);
        asyncOperation.allowSceneActivation = false;
       
        //When the load is still in progress, output the Text and progress bar
        _loadingText.text = "Loading progress: " + (int)((asyncOperation.progress) * 100) + "%";
        //while (true)
            while (!asyncOperation.isDone || !isTokenLoaded || !isDiceLoaded)
            {
            //Output the current progress
            _loadingText.text = "Loading progress: " + (int)((asyncOperation.progress) * 100) + "%";
            // Check if the load has finished
            if (asyncOperation.progress >= 0.9f)
            {
                asyncOperation.allowSceneActivation = true;
            }
            yield return null;
          
        }

        lodingPrompt.SetActive(false);
        isTokenLoaded = false;
        isDiceLoaded = false;
    }

    public void SceneLoad(string sceneName)
    {
        StartCoroutine(SceneLoadIEnumerator(sceneName));
    }

    //this is the General function to open any scene using Loading screen
    IEnumerator SceneLoadIEnumerator(string sceneName)
    {
        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(sceneName);

        asyncOperation.allowSceneActivation = false;
       
        //When the load is still in progress, output the Text and progress bar
        _loadingText.text = "Loading progress: " + (int)((asyncOperation.progress) * 100) + "%";
        while (!asyncOperation.isDone )
        {
            //Output the current progress
            _loadingText.text = "Loading progress: " + (int)((asyncOperation.progress) * 100) + "%";
            // Check if the load has finished
            if (asyncOperation.progress >= 0.9f)
            {
                
                asyncOperation.allowSceneActivation = true;
            }
            yield return null;
        }
        lodingPrompt.SetActive(false);
    }

    private void TokenLoadCompleted()
    {    
        isTokenLoaded = true;
    }
    private void DiceLoadCompleted()
    {
        isDiceLoaded = true;
    }

    private void OnDestroy()
    {
        Events.tokenLoadCompleted -= TokenLoadCompleted;
        Events.diceLoadCompleted -= DiceLoadCompleted;
    }
}
