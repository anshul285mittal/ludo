using UnityEngine;

// This Class manage all the Event Calling funcnality.
public class Events
{
    public delegate void AssetLoadComplete();
    public static AssetLoadComplete tokenLoadCompleted, diceLoadCompleted;
}
