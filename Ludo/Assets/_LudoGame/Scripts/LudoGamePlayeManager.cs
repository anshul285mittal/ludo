using UnityEngine.UI;
using UnityEngine;

// This Class manage Ludo Board Logic like token-moment, turn-managment.
public class LudoGamePlayeManager : MonoBehaviour
{
    [Header("PlayerToken Objects")]
    [SerializeField] private GameObject BlueToken_1_Postion;
    [SerializeField] private Button BlueToken_1_Button;
    [SerializeField] private GameObject BlueToken_1_Highlighter;

    [Header("Dice Object")]
    [SerializeField] private AddressablesDice blueDice;
    [SerializeField] private GameObject[] blueMovementBlocks;

    //Total moment count in the board
    private const int TOTAL_STEPS = 57;

    //This manage Player turn state
    enum PlayerTunr
    {
        RED,
        BLUE,
        YELLOW,
        GREEN
    }
    private PlayerTunr currentTurn=PlayerTunr.BLUE;

   
    private int BlueTokenSteps;

    
    public void BlueToken_1_Select()
    {
        BlueToken_1_Highlighter.SetActive(false);
        BlueToken_1_Button.interactable = false;
        int currentDiceNumber = blueDice.diceScript.diceNumber;
      
        if (currentTurn == PlayerTunr.BLUE && TOTAL_STEPS-BlueTokenSteps> currentDiceNumber)
        {

            // this condition manage the logic: if token in the house than it should require to 6 for initiate the game
            if (BlueTokenSteps == 0 && currentDiceNumber == 6)
            {
                Vector3[] bluePlayer_Path = new Vector3[1];
                bluePlayer_Path[0] = blueMovementBlocks[BlueTokenSteps].transform.position;
                BlueTokenSteps += 1;
                currentTurn = PlayerTunr.BLUE;

                iTween.MoveTo(BlueToken_1_Button.gameObject, iTween.Hash("position", bluePlayer_Path[0], "speed", 225, "time", 2.0f, "easetype", "elastic", "looptype", "none", "oncomplete", "InitializeDice", "oncompletetarget", this.gameObject));
            }
            else
            {
                Vector3[] bluePlayer_Path = new Vector3[currentDiceNumber];

                for (int i = 0; i < currentDiceNumber; i++)
                {
                    bluePlayer_Path[i] = blueMovementBlocks[BlueTokenSteps + i].transform.position;
                }

                BlueTokenSteps += currentDiceNumber;

                if (currentDiceNumber == 6)
                {
                    currentTurn = PlayerTunr.BLUE;
                }
                else
                {
                    //if We have implemented Multiplayer then Code write here

                }

                if (bluePlayer_Path.Length > 1)
                {

                    iTween.MoveTo(BlueToken_1_Button.gameObject, iTween.Hash("path", bluePlayer_Path, "speed", 225, "time", 2.0f, "easetype", "elastic", "looptype", "none", "oncomplete", "InitializeDice", "oncompletetarget", this.gameObject));
                }
                else
                {
                    iTween.MoveTo(BlueToken_1_Button.gameObject, iTween.Hash("position", bluePlayer_Path[0], "speed", 225, "time", 2.0f, "easetype", "elastic", "looptype", "none", "oncomplete", "InitializeDice", "oncompletetarget", this.gameObject));
                }
            }
        }
        blueDice.diceScript.DiceIntractable = true;
    }

    // this function manage if dice allow to roll or not.
    public void DiceRolled()
    {
        int currentDiceNumber = blueDice.diceScript.diceNumber;
        if (currentTurn == PlayerTunr.BLUE && TOTAL_STEPS - BlueTokenSteps > currentDiceNumber)
        {
            if (BlueTokenSteps == 0)
            {
                if (currentDiceNumber != 6) {
                    blueDice.diceScript.DiceIntractable = true;
                }
                else
                {
                    blueDice.diceScript.DiceIntractable = false;
                    BlueToken_1_Highlighter.SetActive(true);
                    BlueToken_1_Button.interactable = true;
                }
            }
            else
            {
                blueDice.diceScript.DiceIntractable = false;
                BlueToken_1_Highlighter.SetActive(true);
                BlueToken_1_Button.interactable = true;
            }
           
        }
        else
        {
            blueDice.diceScript.DiceIntractable = true;
        }
           
        //if We have implemented Multiplayer then Code write here
        switch (currentTurn)
        {
            case PlayerTunr.RED:
                break;
            case PlayerTunr.BLUE:
                break;
            case PlayerTunr.YELLOW:
                break;
            case PlayerTunr.GREEN:
                break;

        }
    }

    //Reset button will intract this function.
    public void ResetToken()
    {
        BlueTokenSteps = 0;
        iTween.MoveTo(BlueToken_1_Button.gameObject, iTween.Hash("position", BlueToken_1_Postion.transform.position, "speed", 225, "time", 2.0f, "easetype", "elastic", "looptype", "none", "oncomplete", "InitializeDice", "oncompletetarget", this.gameObject));
    }

    // menu button will intract this function.
    public void OpenMenu()
    {
        GameManager.Instance.SceneLoad("MainMenu");
    }
}
