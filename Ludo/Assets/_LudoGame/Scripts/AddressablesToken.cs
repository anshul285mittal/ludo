
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AddressableAssets;
using UnityEngine.AddressableAssets.ResourceLocators;
using UnityEngine.ResourceManagement.AsyncOperations;

//Get token sprite from server
public class AddressablesToken : MonoBehaviour
{
    [SerializeField] private AssetReferenceSprite[] tokenAssetsReference;
    [SerializeField] private Image tokenImage;

    private Sprite _sprite;
   
    void Start()
    {
        //register the event when Adressables get initialize
        Addressables.InitializeAsync().Completed += AddressablesManager_Completed;
    }

    private void AddressablesManager_Completed(AsyncOperationHandle<IResourceLocator> obj)
    {
        
        tokenAssetsReference[0].LoadAssetAsync<Sprite>().Completed += (sprite) => {
            // When token sprite get load then assigne to token.
            _sprite = sprite.Result;
            tokenImage.sprite = _sprite;

            // Trigger the event when successful token created.
            Events.tokenLoadCompleted?.Invoke();
        };
    }

    private void OnDestroy()
    {
        //clean memory.
        tokenAssetsReference[0].ReleaseAsset();
    }
}
