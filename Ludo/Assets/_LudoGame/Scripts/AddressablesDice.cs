using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.AddressableAssets.ResourceLocators;
using UnityEngine.Events;
using UnityEngine.ResourceManagement.AsyncOperations;

//Get Dice prefabs from server
public class AddressablesDice : MonoBehaviour
{
    [SerializeField] private AssetReference dice1AssetReference;
    [SerializeField] private AssetReference dice2AssetReference;
    [SerializeField] UnityEvent diceRolledEvent; 

    public DiceScript diceScript { get; private set; }
   
    void Start()
    {
        //register the event when Adressables get initialize
        Addressables.InitializeAsync().Completed += AddressablesManager_Completed;
    }

    private void AddressablesManager_Completed(AsyncOperationHandle<IResourceLocator> obj)
    {
       
        dice1AssetReference.InstantiateAsync().Completed += (go) =>
        {
            // When Dice prefab get instantiate then assigne initial values.
            diceScript = go.Result.GetComponent<DiceScript>();
            diceScript.transform.parent = transform;
            diceScript.transform.localPosition = Vector3.zero;
            diceScript.transform.localScale = Vector3.one;

            // assing event when dice get clicked.
            diceScript.diceButton.onClick.AddListener(delegate { diceRolledEvent?.Invoke(); });

            // Trigger the event when successful dice created.
            Events.diceLoadCompleted?.Invoke();
        };
    }

    private void OnDestroy()
    {
        //clean memory.
        dice1AssetReference.ReleaseInstance(diceScript.gameObject);
    }
}
