using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

//Manage API calls for Random number genration.
public class RandomDataApi: Singleton<RandomDataApi>
{
    //this variable store random Numbers in buffer.
    //Buffer help to prevent frequent calls for API.
    private Queue<int> randomNumberBuffer;

    private const string API_URL = "https://www.random.org/integers/";
    private void Start()
    {
        randomNumberBuffer = new Queue<int>();
        StartCoroutine(RequestNumber());
    }
    public int GetRandomNumber()
    {
        int randomNumber;
        if (randomNumberBuffer.Count != 0)
        {
            if (randomNumberBuffer.Count == 1)
            {
                StartCoroutine(RequestNumber());
            }
            randomNumber = randomNumberBuffer.Dequeue();
        }
        else
        {
            // This is in exceptional cases when API is not working.
            randomNumber = UnityEngine.Random.Range(1, 7);
        }
       
        return randomNumber;
    }
   
    IEnumerator RequestNumber()
    {
        string variables = "?num=10&min=1&max=6&col=1&base=10&format=plain&rnd=new";
        UnityWebRequest www = UnityWebRequest.Get(API_URL + variables);
        yield return www.SendWebRequest();

        if (www.result != UnityWebRequest.Result.Success)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log("Data" + www.downloadHandler.text);
            var allNumber = www.downloadHandler.text.Split(new string[] { Environment.NewLine },StringSplitOptions.None);

            // Convert string data to int and store into the buffer queue.
            for (int i = 0; i< allNumber.Length; i++)
            {
                int number = -1;
                try
                {
                    number = int.Parse(allNumber[i]);
                }
                catch (FormatException)
                {
                    break;
                }
                randomNumberBuffer.Enqueue(number);
            }
           
        }
    }
}


