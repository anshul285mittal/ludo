using UnityEngine.UI;
using UnityEngine;

//Manage All dice functionality
public class DiceScript : MonoBehaviour
{
    [SerializeField] private Image diceImage;
                     public Button diceButton;
    [SerializeField] private GameObject highlighter;
    [SerializeField] private Sprite[] diceSprites;
    [SerializeField] private Animator diceAnimator;

  
    public int diceNumber { get; private set; }

    //This get set variable helps to manage dice intractable functionality from outer calsses
    public bool DiceIntractable
    {
        get { return diceButton.interactable; }
        set { diceButton.interactable=value;
            highlighter.SetActive(value);
        }
    }

    //this function will call when dice button get clicked.
    public void Roll()
    {
        diceNumber = GetRandomNumber();
        diceAnimator.enabled = true;
    }

    //this function will call from animation clip as event.
    public void DiceRollEnd()
    {
        diceAnimator.enabled = false;
        diceImage.sprite = diceSprites[diceNumber - 1];
    }

    private int GetRandomNumber()
    {
        return RandomDataApi.Instance.GetRandomNumber();
       // return 6;
    }
}
