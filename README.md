
##Ludo Unity3D Project README

Welcome to the Ludo Unity3D project, provided by Tamatem. This README will provide an overview of the project, its components, and how to set it up and use it.


## Description
In this Unity3D project, you'll find two Addressable assets that are used for creating a Ludo game:

# Dice Prefab:
This asset contains the necessary sprites and animations for simulating dice rolls.
Due to its size, the entire prefab has been uploaded to a remote server.

# Token Sprite:
This asset is a simple sprite used for Ludo game tokens.
It has been uploaded to the server but doesn't require complex animations, so only the sprite is stored remotely.

# Server Details
Server Name: We are using an Amazon S3 bucket for hosting the remote assets.
Server URL: https://d-debyy.s3.amazonaws.com/LudoAndroid/[BuildTarget]

# Random Number Generation API
To handle random number generation in the Ludo game, we've implemented an API using UnityWebRequest. This API is responsible for obtaining random numbers and ensuring fair gameplay.

API URL: https://www.random.org/integers


## Build Locations
# Addressable Files: The Addressable assets are saved within the project itself, specifically in the "ServerData" folder.

#Android Build APK: After building the project, the Android APK will be located in the "Build" folder.


## Project Structure
This project consists of two scenes:

# MainMenu:

The starting point of the game, where players can initiate gameplay.


# Game Play:
The scene where the actual Ludo game takes place. Players can roll the dice, move tokens, and reset the token position.
Player can start game when first Get 6 in dice roll.

# Setup Instructions
To set up and run the Ludo Unity3D project, follow these steps:

1.Clone this repository to your local machine.

2.Open Unity3D.

3.From the Unity3D menu, choose "Open Project" and select the project's root folder.

4.Ensure you have the required Unity modules(IOS and Android Support) and versions(2021.3.13f1) installed. You might need to update the project to match your Unity version if prompted.

5. Configure the server URL for the addressable assets in the project settings.

6.Build and run the project on your target platform.


# Important Notes
This project utilizes remote assets for optimal performance and to reduce the initial download size.

Make sure you have a stable internet connection to access the remote assets and random number generation API.

Feel free to modify and extend this project as needed to meet your specific requirements or to enhance the Ludo game.

# License
This project's license and usage are subject to the terms and conditions set forth by Tamatem. Please refer to the provided license documentation for more information.

Have fun developing and playing the Ludo game! If you have any questions or encounter issues, don't hesitate to reach out for support.